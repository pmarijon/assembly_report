#!/usr/bin/env python

import sys
import csv
import begin

# Upgrade the field size
csv.field_size_limit(sys.maxsize)

from Bio import SeqIO
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from Bio.Alphabet import DNAAlphabet

@begin.start
def main(gfa: "GFA filename",
         fastq: "fastq filename"):

    with open(fastq, "w") as fastqfile:
        with open(gfa) as gfafile:
            gfareader = csv.reader(gfafile, delimiter='\t')
            for row in gfareader:
                if row[0] == "S" : #and len(row[2]) < 10:
                    SeqIO.write(SeqRecord(Seq(row[2], DNAAlphabet()), id=row[1],
                                          letter_annotations={
                                              "solexa_quality": [40] * len(row[2])}
                                          ), fastqfile, "fastq")
