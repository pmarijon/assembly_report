#!/usr/bin/env python

import csv
import begin

from Bio import SeqIO

@begin.start
def main(first_fastq: "A fastq file",
         second_fastq: "A fastq file",
         out_csv: "The file where result is write"):
    """If id (@[this is id]\s) is present in 2 fastq id color is green else is 
    read"""

    first_id = set()
    for record in SeqIO.parse(first_fastq, "fastq"):
        first_id.add(record.id)

    second_id = set()
    for record in SeqIO.parse(second_fastq, "fastq"):
        second_id.add(record.id)

    with open(out_csv, "w") as outfile:
        out_csv_writer = csv.writer(outfile, delimiter=',')
        out_csv_writer.writerow(["Node name", "color"])

        for seq_id in first_id & second_id:
            out_csv_writer.writerow([seq_id, "#08ff00"])

        for seq_id in first_id ^ second_id:
            out_csv_writer.writerow([seq_id, "#ee00ff"])
