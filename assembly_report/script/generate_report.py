#!/usr/bin/env python3

# cmd line
import begin

# convert png in base64 version
import base64

# report template
from jinja2 import FileSystemLoader
from jinja2 import Environment

# read biological file
from Bio import SeqIO
import re
import os
import csv

import pandas

from collections import defaultdict

from itertools import combinations

try:
    from os import scandir, walk
except ImportError:
    from scandir import scandir, walk

def list_file_in_dir(path, reg):
    for entry in scandir(path):
        if entry.is_file() and re.search(reg, entry.name):
            yield os.path.abspath(entry.path)

# For support graph generation without X server
# source http://matplotlib.org/faq/howto_faq.html#matplotlib-in-a-web-application-server
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches

from matplotlib import cm
from matplotlib import colors
from matplotlib.colors import rgb2hex

from io import BytesIO

from bs4 import BeautifulSoup

import networkx as nx

##########
# CODE DUPLICATION FROM script/graph_projection_gfa.py
##########
def color_map(n: int):
    
    color_norm = colors.Normalize(vmin=0, vmax=n-1)
    scalar_map = cm.ScalarMappable(norm=color_norm, cmap='rainbow')

    def map_index_to_rgb_color(index: int):
        return scalar_map.to_rgba(index)

    for i in range(n):
        yield map_index_to_rgb_color(i)

def build_contigs_graph(nodes, edges):
    nodes_str = ""
    edges_str = ""

    G = nx.Graph()

    edge_tig2reads = defaultdict(set)
    for node, tig in nodes:
        G.add_node(node, label=tig)
        edge_tig2reads[tig].add(node)

    for tig, nodes in edge_tig2reads.items():
        nodes = list(nodes)
        if len(nodes) == 1:
            G.add_node(nodes[0]+"_", label=str(tig))
            G.add_edge(nodes[0], nodes[0]+"_", type="tig", label=str(tig))
        else:
            G.add_edge(nodes[0], nodes[1], type="tig", label=str(tig))

    for node1, node2, length in edges:
        if G.has_edge(node1, node2):
            G.edges[node1, node2]["label"] += " " + length
            G.edges[node1, node2]["id"] += ";" + node1+","+node2
            if G.edges[node1, node2]["length"] > int(length):
                G.edges[node1, node2]["length"] == int(length)
        else:
            G.add_edge(node1, node2, label=length, type="path", id=node1+","+node2, length=int(length))
    
    if G.number_of_nodes() < 200:
        H = G.copy()
        cliques = list()
        while H.number_of_nodes() != 0:
            clique = next(nx.find_cliques(H))
            cliques.append(clique)
            for n in clique:
                H.remove_node(n)

        for clique, color in zip(cliques, color_map(len(cliques))):
            for node in clique:
                G.nodes[node]["color"] = rgb2hex(color)

    for node, data in G.nodes(data=True):
        if "color" in data:
            nodes_str += "{id: '"+node+"', label: '"+data["label"]+"', color:'"+data["color"]+"'},\n"
        else:
            nodes_str += "{id: '"+node+"', label: '"+data["label"]+"'},\n"

    for e1, e2, data in G.edges(data=True):
        if data["type"] == "path":
            edges_str += "{from: '"+e1+"', to: '"+e2+"', label: '"+data["label"]+"', id: '"+data["id"]+"', length: '"+str(data["length"])+"'},\n"
        else:
            edges_str += "{from: '"+e1+"', to: '"+e2+"', label: "+data["label"]+", width: 10, length: 1},\n"

    return nodes_str, edges_str

def generate_legend(path):
    patch_list = list()
    label_list = list()
    
    with open(path, "r") as fd:
        reader = csv.DictReader(fd)
        for row in reader:
            if not row["color"] == "#000000FF":
                patch_list.append(mpatches.Patch(color=row["color"], label=row["Tig name"]))
                label_list.append(row["Tig name"])

    plt.legend(handles=patch_list, labels=label_list, loc="center")
    
    plt.axis("off")

    figfile = BytesIO()
    plt.savefig(figfile, format="png", bbox_inches='tight')

    figfile.seek(0)
    
    return figfile

def _load_canu_only_info(param, canu_contig_on_paf, canu_contig_on_paf_legend):

    param["canu_contig_paf"] = str(base64.b64encode(open(canu_contig_on_paf, "rb").read()))[2:-1]
    param["legend"] = str(base64.b64encode(generate_legend(canu_contig_on_paf_legend).read()))[2:-1]


def _load_canu_info(param, canu_fasta,
                    canu_bog_gfa, canu_bog, canu_paf, canu_contig, canu_unitig,
                    canu_contig_report_csv, canu_contig_report_png,
                    path_search_result, list_input_file,):

    # generate stat from canu_fasta
    total_len = 0
    nb_contig = 0
    for seq_record in SeqIO.parse(canu_fasta, "fasta"):
        total_len += len(seq_record.seq)
        nb_contig += 1

    nb_seq = 0
    with open(canu_bog_gfa) as canu_bog_file:
        for line in canu_bog_file:
            if line.startswith("S"):
                nb_seq += 1
    

    canu_stat = dict()
    canu_stat["Number of read"] = nb_seq
    canu_stat["Number of contig"] = nb_contig
    canu_stat["Total size"] = total_len

    # Build dict for complet template
    param["list_input"] = list_input_file
    param["canu_bog"] = str(base64.b64encode(open(canu_bog, "rb").read()))[2:-1]
    param["canu_paf"] = str(base64.b64encode(open(canu_paf, "rb").read()))[2:-1]
    param["canu_contig"] = str(base64.b64encode(open(canu_contig, "rb").read()))[2:-1]
    param["canu_unitig"] = str(base64.b64encode(open(canu_unitig, "rb").read()))[2:-1]

    param["canu_stat"] = canu_stat
    param["canu_contig_report_csv"] = pandas.read_csv(canu_contig_report_csv).to_html(index=False)
    param["canu_contig_report_png"] = str(base64.b64encode(open(canu_contig_report_png, "rb").read()))[2:-1]

    # build path search
    nodes = set()
    edges = set()

    canu_path_readA = list()
    canu_path_readB = list()
    canu_path_ok = defaultdict(dict)
    canu_path_len = defaultdict(dict)
    canu_path_svg = defaultdict(dict)
    with open(path_search_result, "r") as path_file:
        reader = csv.DictReader(path_file)
        for row in reader:
            nodes.add((row["readA"][:-1], row["tigA"]))
            nodes.add((row["readB"][:-1], row["tigB"]))
            edges.add((row["readA"][:-1], row["readB"][:-1], row["path_len"]))
            
            canu_path_readA.append((row["readA"], row["tigA"]))
            canu_path_readB.append((row["readB"], row["tigB"]))
            canu_path_ok[row["readA"]][row["readB"]] = row["path_find"]
            canu_path_len[row["readA"]][row["readB"]] = row["path_len"]
            canu_path_svg[row["readA"]][row["readB"]] = str(base64.b64encode(open(row["image_path"], "rb").read()))[2:-1]
   
    param["canu_contig_graph_node"], param["canu_contig_graph_edge"] = build_contigs_graph(nodes, edges)

    param["canu_path_readA"] = set(canu_path_readA)
    param["canu_path_readB"] = set(canu_path_readB)
    param["canu_path_ok"] = canu_path_ok
    param["canu_path_len"] = canu_path_len
    param["canu_path_svg"] = canu_path_svg

def _load_minimap_info(param, minimap_paf, canu_contig_on_minimap,
                       canu_contig_on_minimap_legend, mini_path_search_result):

    # generate stat from minimap
    reads = set()
    nb_line = 0
    with open(minimap_paf) as mini_paf :
        rows = csv.reader(mini_paf, delimiter="\t")
        for row in rows:
            reads.add(row[0])
            reads.add(row[5])
            nb_line += 1

    canu_contig_on_minimap_legend = generate_legend(canu_contig_on_minimap_legend)

    minimap_stat = dict()
    minimap_stat["Number of overlap"] = nb_line
    minimap_stat["Number of reads"] = len(reads)

    param["minimap_stat"] = minimap_stat
    param["canu_contig_mini"] = str(base64.b64encode(open(canu_contig_on_minimap, "rb").read()))[2:-1]
    param["legend"] = str(base64.b64encode(canu_contig_on_minimap_legend.read()))[2:-1]
    
    # build path search
    nodes = set()
    edges = set()

    minimap_path_readA = list()
    minimap_path_readB = list()
    minimap_path_ok = defaultdict(dict)
    minimap_path_len = defaultdict(dict)
    minimap_path_svg = defaultdict(dict)
    with open(mini_path_search_result, "r") as path_file:
        reader = csv.DictReader(path_file)
        for row in reader:
            nodes.add((row["readA"][:-1], row["tigA"]))
            nodes.add((row["readB"][:-1], row["tigB"]))
            edges.add((row["readA"][:-1], row["readB"][:-1], row["path_len"]))
            
            minimap_path_readA.append((row["readA"], row["tigA"]))
            minimap_path_readB.append((row["readB"], row["tigB"]))
            minimap_path_ok[row["readA"]][row["readB"]] = row["path_find"]
            minimap_path_len[row["readA"]][row["readB"]] = row["path_len"]
            minimap_path_svg[row["readA"]][row["readB"]] = str(base64.b64encode(open(row["image_path"], "rb").read()))[2:-1]


    param["minimap_contig_graph_node"], param["minimap_contig_graph_edge"] = build_contigs_graph(nodes, edges)
    
    param["minimap_path_readA"] = set(minimap_path_readA)
    param["minimap_path_readB"] = set(minimap_path_readB)
    param["minimap_path_ok"] = minimap_path_ok
    param["minimap_path_len"] = minimap_path_len
    param["minimap_path_svg"] = minimap_path_svg

def _load_miniasm_info(param, miniasm_gfa, miniasm_png):

    # generate stat from miniasm 
    total_len = 0
    nb_seq = 0
    with open(miniasm_gfa, "r") as asm_gfa :
        for line in asm_gfa:
            if line.startswith("S"):
                len_segment = re.findall("LN:i:(\d+)\s", line)
                total_len += int(len_segment[0]) if len(len_segment) > 0 else int(0)
                nb_seq += 1

    miniasm_stat = dict()
    miniasm_stat["Number of contig"] = nb_seq
    miniasm_stat["Total size"] = total_len

    param["miniasm_stat"] = miniasm_stat
    param["miniasm_gfa"] = str(base64.b64encode(open(miniasm_png, "rb").read()))[2:-1]


@begin.subcommand
def canu_only(template_path, canu_fasta, canu_bog_gfa,
              canu_bog, canu_paf, canu_contig, canu_unitig, canu_contig_on_paf,
              canu_contig_on_paf_legend, canu_contig_report_csv,
              canu_contig_report_png, path_search_result, list_input_file, out_filename):

    # Create jinja2 env
    env = Environment(loader=FileSystemLoader(template_path))
    template = env.get_template('canu_only.jinja2.html')

    param = dict()

    _load_canu_info(param, canu_fasta, canu_bog_gfa,
              canu_bog, canu_paf, canu_contig, canu_unitig, canu_contig_report_csv,
              canu_contig_report_png, path_search_result, list_input_file)

    _load_canu_only_info(param, canu_contig_on_paf, canu_contig_on_paf_legend)

    with open(out_filename, "wb") as f :
        f.write(template.render(param).encode("utf-8"))

@begin.subcommand
def canu_minimap(template_path, canu_fasta, canu_bog_gfa,
                 canu_bog, canu_paf, canu_contig, canu_unitig,
                 canu_contig_on_minimap, canu_contig_on_minimap_legend,
                 canu_contig_report_csv, canu_contig_report_png, minimap_paf,
                 minimap_png, canu_path_search_result, mini_path_search_result,
                 list_input_file, out_filename):

        # Create jinja2 env
    env = Environment(loader=FileSystemLoader(template_path))
    template = env.get_template('canu_minimap.jinja2.html')

    param = dict()

    _load_canu_info(param, canu_fasta, canu_bog_gfa,
              canu_bog, canu_paf, canu_contig, canu_unitig, canu_contig_report_csv,
              canu_contig_report_png, canu_path_search_result, list_input_file)

    _load_minimap_info(param, minimap_paf, canu_contig_on_minimap, canu_contig_on_minimap_legend, mini_path_search_result)

    with open(out_filename, "wb") as f :
        f.write(template.render(param).encode("utf-8"))

@begin.subcommand
def canu_miniasm(template_path, canu_fasta, canu_bog_gfa,
                 canu_bog, canu_paf, canu_contig, canu_unitig,
                 canu_contig_on_minimap, canu_contig_on_minimap_legend,
                 canu_contig_report_csv, canu_contig_report_png, minimap_paf,
                 minimap_png, miniasm_gfa, miniasm_png, canu_path_search_result,
                 mini_path_search_result, list_input_file, out_filename):

    # Create jinja2 env
    env = Environment(loader=FileSystemLoader(template_path))
    template = env.get_template('canu_miniasm.jinja2.html')

    param = dict()

    _load_canu_info(param, canu_fasta, canu_bog_gfa,
              canu_bog, canu_paf, canu_contig, canu_unitig, canu_contig_report_csv,
              canu_contig_report_png, canu_path_search_result, list_input_file)


    _load_minimap_info(param, minimap_paf, canu_contig_on_minimap, canu_contig_on_minimap_legend, mini_path_search_result)

    _load_miniasm_info(param, miniasm_gfa, miniasm_png)

    with open(out_filename, "wb") as f :
        f.write(template.render(param).encode("utf-8"))


@begin.start
def main():
    pass
