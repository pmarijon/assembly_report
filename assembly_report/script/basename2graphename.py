#!/usr/bin/env python3

import csv
import begin
import pandas

from Bio import SeqIO

def parse_canu_read_name(filename, invert):

    index = (0) if invert else (1)
  
    id2readname = dict()
    with open(filename, "r") as in_csv:
        reader = csv.reader(in_csv, delimiter='\t')
        for row in reader:
                id2readname[row[0]] = row[1].split(" ")[0]
    
    id2readname = {v: k for k, v in id2readname.items()} if invert else id2readname
    
    return id2readname


def build_correspondance_function(filename, invert):
    id2readname = parse_canu_read_name(filename, invert)

    if not invert:
        def corres_func(iid):
            return str(id2readname[clean_read_name(iid)])
    else:
        def corres_func(iid):
            return id2readname[clean_read_name(iid)]

    return corres_func

def clean_read_name(name):
    return name.replace("read", "").lstrip('0')

@begin.subcommand
def paf(paf_file: "paf file generate by canu",
        read_name: "path to gatekeeper readName.txt",
        new_paf_file: "paf file with node id replace by canu id",
        invert: "set this option if you want replace the canu by node id" = False
        ):

        id_map = build_correspondance_function(read_name, invert)
        
        with open(new_paf_file, "w") as output:
            paf_writer = csv.writer(output, delimiter="\t")
            with open(paf_file, "r") as paf_input:
               paf_parser = csv.reader(paf_input, delimiter="\t")
               for row in paf_parser:
                   row[0] = id_map(row[0])
                   row[5] = id_map(row[5])
                   paf_writer.writerow(row)

@begin.subcommand
def gfa(gfa_file: "gfa file generate by canu",
        read_name: "path to gatekeeper readName.txt",
        new_gfa_file: "gfa file with node id replace by canu id",
        invert: "set this option if you want replace the canu by node id" = False
        ):

        id_map = build_correspondance_function(read_name, invert)
        
        with open(new_gfa_file, "w") as output:
            gfa_writer = csv.writer(output, delimiter="\t")
            with open(gfa_file, "r") as gfa_input:
               gfa_parser = csv.reader(gfa_input, delimiter="\t")
               for row in gfa_parser:
                    if row[0] == "S":
                       row[1] = id_map(row[1])
                    elif row[0] == "L":
                        row[1] = id_map(row[1])
                        row[3] = id_map(row[3])
                    elif row[0] == "C":
                        row[1] = id_map(row[1])
                        row[3] = id_map(row[3])
                    gfa_writer.writerow(row)

@begin.subcommand
def fastq(fastq_file: "fastq file generate by canu gatekeeperDumpFASTQ",
          read_name: "gakekeeper read name",
          new_fastq_file: "fastq file with comment replace by BOG id",
          invert: "set this option if you want replace the canu by node id" = False
          ):
   
        id_map = build_correspondance_function(read_name, invert)

        with open(new_fastq_file, "w") as output:
            for record in SeqIO.parse(fastq_file, "fastq"):
                if not invert:
                    record.id = id_map(record.id)
                else:
                    record.id = id_map(record.id)
                record.name = ""
                record.description = ""
                SeqIO.write(record, output, "fastq")

@begin.start
def main():
    pass
