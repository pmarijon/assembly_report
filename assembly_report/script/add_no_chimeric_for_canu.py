#!/usr/bin/env python3

import re

import begin

def line_in_set(line, intersection):
    l = line.split("\t")
    return l[0] in intersection or l[5] in intersection

@begin.start
def main(yacrd_log, canu_paf, minimap_paf_no_chimera, minimap_paf_chimera, minimap_good):

    # name chimeric
    chimeric_name = set()
    with open(yacrd_log) as fh:
        for line in fh:
            chimeric_name.add(re.split(r"\t", line)[1])
  
    # canu paf
    canu_name = set()
    with open(canu_paf) as fh:
        for line in fh:
            line = line.split("\t")
            canu_name.add(line[0])
            canu_name.add(line[5])

    intersection = chimeric_name & canu_name

    with open(minimap_good, "w") as out:
        with open(minimap_paf_no_chimera) as no_chim:
           for line in no_chim:
                out.write(line)

        with open(minimap_paf_chimera) as chim:
            for line in chim:
                if line_in_set(line, intersection):
                    out.write(line)
