# This repo is DEPRECATED. Please use knot, the successor of assembly_report [https://gitlab.inria.fr/pmarijon/knot](https://gitlab.inria.fr/pmarijon/knot) 

---

Assembly_report is an assembly analyzer and debugger for Canu and mini{map|asm}.

You can find an exemple of output [her](https://gitlab.inria.fr/pmarijon/assembly_report/raw/master/demo/report.html) 

# Input

The tool takes as input an existing Canu assembly. Optionnally, it can also take as input a set of reads and generate the Canu assembly itself (with default parameters), as well as a miniasm assembly for comparison.

# Output

Assembly_report produces a HTML file with several components:

* SG: an overlap graph with no transtive arc and containment based on overlap graph seen by Canu and miniasm
* BOG: the best overlap graph of Canu (intermediate representation between the SG and the assembly)
* graph projection: Canu's contigs projected onto the minimap SG
* contig extremities analysis: analysis of read overlaps at each contig extremity
* paths between contig extremities: finds paths in the Canu SG, as well as the minimap SG, between reads at contigs extremities

![graph projection explain](images/graph_projection_explain.png)

You can find and example output here [report.html](https://gitlab.inria.fr/pmarijon/assembly_report/raw/master/demo/report.html).
Just save this file and open it in a browser (it won't display on gitlab directly).

![gif present basic report out](images/ex_data_assembly_report.gif)

# Replicability

An additional gitlab page is available to enable replication of our manuscript results: [Assembly graph analysis of fragmented long read bacterial genome assemblies](https://gitlab.inria.fr/pmarijon/assembly_graph_analysis_of_fragmented_long_read_bacterial_genome_assemblies_repetition)

# Installation and Usage

* [Installation](#installation)
    * [Pre-requisites](#pre-requisites)
    * [With conda](#main-installation-with-conda) *(recommended)*
    * [Without conda](#main-installation-without-conda)
* [Usage](#usage)
    * [Basic usage](#basic-usage)
    * [Demo dataset](#demo-dataset)
    * [Warning](#warning)
* [Update](#update)
    * [Conda installation](#conda-installation)
    * [Non-conda installation](#non-conda-installation)
* [Contact](#contact)

# Installation

## Pre-requisites

- [Bandage](https://github.com/rrwick/Bandage) (note: development branch). 
Until a new version of Bandage that contains recent developments is released, the installations instruction for Bandage are as follows:

```
git clone https://github.com/rrwick/Bandage.git
cd Bandage
git checkout development
qmake
make -j 16
```

And make sure that the `Bandage` binary is in your PATH environment.

## Main installation, with conda

Recommended solution (takes ~ 10 minutes but fairly automatic)

```
wget https://gitlab.inria.fr/pmarijon/assembly_report/raw/master/conda_env.yml
conda env create -f conda_env.yml
```

### Manage Conda environement

Activate environement :

```
source activate assembly_report
```

Unactivate environement :

```
source deactivate assembly_report
```

## Main installation, without Conda

### Pre-requisites

- python 3
- [Bandage](https://github.com/rrwick/Bandage) (same instructions as above)
- [yacrd](https://github.com/natir/yacrd)
- graphviz
- pygraphviz
- canu (with all exectuable in path)

## Instruction

```
pip3 install -r https://gitlab.inria.fr/pmarijon/assembly_report/raw/master/install.txt
```

Replace `pip3` by `pip` if your default Python version is 3.

# Usage

## Basic usage

1) Assume that Canu was run with `canu -p my_assembly -d assembly_folder`. Do:

2) `cd assembly_folder/`

3) `assembly_report`

This will generate a report in `assembly_report/` directory.

For a more complete report, please specify the minimap PAF file
and the miniasm GFA file using the `-m` and `-M` options respectively.


Full command line usage:

```
usage: assembly_report [-h] [-i INPUT] [-o OUTPUT] [-p PROJECT] [-m MINIMAP]
                       [-M MINIASM] [-v] [-c] [-t THREAD]

optional arguments:
  -h, --help            show this help message and exit
  -i INPUT, --input INPUT
                        canu directory (default: ./)
  -o OUTPUT, --output OUTPUT
                        output directory (default: ./assembly_report)
  -p PROJECT, --project PROJECT
                        project name give to canu, default auto detect use
                        option to overide (default: None)
  -m MINIMAP, --minimap MINIMAP
                        path to minimap paf output (default: None)
  -M MINIASM, --miniasm MINIASM
                        path to miniasm gfa output (default: None)
  -v, --verbose         verbose (default: False)
  -c, --clean           clean all file generated (default: False)
  -t THREAD, --thread THREAD
                        number of thread usable (default: 1)
```


## Demo dataset

You can download a html report generate on this demo data set [her](https://gitlab.inria.fr/pmarijon/assembly_report/raw/master/demo/report.html)

You can download a [test dataset](https://gitlab.inria.fr/pmarijon/assembly_report/raw/master/demo/demo_dataset.tar.bz2), and try assembly_report with these commands:

```
tar xvfj demo_dataset.tar.bz2
cd demo_dataset/assembly/canu
assembly_report # for canu only run, result are store in canu/assembly_report
cd ..
assembly_report -i canu -m minimap/minimap.paf # for canu and minimap run
assembly_report -i canu -m minimap/minimap.paf -M miniasm/miniasm.gfa # for canu, minimap and miniasm run
```

# Update

## Conda installation

The recommended way to update this tool is to remove the conda environement and reinstall it :

```
source deactivate assembly_report
conda env remove -n assembly_report
wget https://gitlab.inria.fr/pmarijon/assembly_report/raw/master/conda_env.yml
conda env create -f conda_env.yml
```

## Non-conda installation

Run : 

```
pip3 install --upgrade git+https://gitlab.inria.fr/pmarijon/paf2gfa.git#egg=paf2gfa
pip3 install --upgrade git+https://gitlab.inria.fr/pmarijon/path_in_gfa.git#egg=path_in_gfa
pip3 install --upgrade git+https://gitlab.inria.fr/pmarijon/assembly_report.git#egg=assembly_report
```

Replace `pip3` by `pip` if your default Python version is 3.

# Contact

For question or bug report send e-mail to pierre.marijon@inria.fr

