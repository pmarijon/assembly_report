#!/usr/bin/env python3

import csv
import begin

import networkx as nx

import re

from matplotlib import cm
from matplotlib import colors
from matplotlib.colors import rgb2hex

def correct_name(name):
    return re.sub(r"read0+", "", name)

def read_gfa(filename):

    G = nx.Graph()

    with open(filename, "r") as gfafile:
        gfaparser = csv.reader(gfafile, delimiter="\t")
        for row in gfaparser:
            if row[0] == "S":
                G.add_node(correct_name(row[1]), color="#000000FF")
            elif row[0] == "L":
                if not G.has_node(row[1]):
                    G.add_node(correct_name(row[1]), color="#000000FF")
                if not G.has_node(row[3]):
                    G.add_node(correct_name(row[3]), color="#000000FF")
                
                G.add_edge(correct_name(row[1]), correct_name(row[3]), nothing="empty")

    return G

def color_map_gene(n: int):
    
    color_norm = colors.Normalize(vmin=0, vmax=n-1)
    scalar_map = cm.ScalarMappable(norm=color_norm, cmap='rainbow')

    def map_index_to_rgb_color(index: int):
        return scalar_map.to_rgba(index)

    return map_index_to_rgb_color

#########
# Warning duplication
#########
# Original code come from assembly_report/canu_utils
from collections import defaultdict
def read_contig_layout(filename):
    import csv

    read2info = defaultdict(dict)
    tig2extremity = defaultdict(set)
    with open(filename, "r") as fd:
        iterator = csv.reader(fd, delimiter="\t")
        
        key = iterator.__next__()
        key.pop(0)

        actual_tig = 0
        for row in iterator:

            if row[3] > row[4]:
                row[3], row[4] = row[4], row[3]

            # index all info
            for k, v in zip(key, row[1:]):
               read2info[row[0]][k] = v
               
            #index extremity
            if actual_tig == 0:
                actual_tig = row[1]
                actual_begin = row[0]
                actual_end = row[0]
 
            if actual_tig != row[1]:
                tig2extremity[actual_tig] = (actual_begin, actual_end)
                actual_tig = row[1]
                actual_begin = row[0]
                actual_end = row[0]   

            if float(row[4]) > float(read2info[actual_end]["end"]):
                actual_end = row[0]

    return read2info, tig2extremity

##########
# WARNING
#########
#
# Big code duplication with script basename2graphename.py
import csv
def parse_canu_read_name(filename, invert):

    index = (0) if invert else (1)
  
    id2readname = dict()
    with open(filename, "r") as in_csv:
        reader = csv.reader(in_csv, delimiter='\t')
        for row in reader:
            id2readname[row[0]] = row[1].split(" ")[0]
    
    id2readname = {v: k for k, v in id2readname.items()} if invert else id2readname
    
    return id2readname

def clean_read_name(name):
    return name.replace("read", "").lstrip('0')


class FakeDict:
    def __getitem__(self, key):
        return key

    def __contains__(self, item):
        return True

@begin.start
def main(first_gfa: "A gfa file",
         second_gfa: "A gfa file",
         out_csv: "The file where result is write",
         out_legend: "The file tig 2 color correspondance is write",
         canu_tig_file: "canu readToTig, if is empty no used" = "",
         read_name: "canu readName, if is empty no used" = "",
         canu_mode: "didn't make correspondance" = False):
    """
    Generate CSV with link node id to color
    One color per composante of first gfa, node only in the second gfa is transparent
    """

    first_graph = read_gfa(first_gfa)
    second_graph = read_gfa(second_gfa)

    connected_components = [c for c in nx.connected_components(first_graph)]
    color_map = color_map_gene(len(connected_components))
    print(len(connected_components))
    if canu_tig_file != "":
        read2info, _ = read_contig_layout(canu_tig_file)
        id2name = parse_canu_read_name(read_name, True)
   
    if canu_mode:
        id2name = FakeDict()

    i = 0
    for c in connected_components:
        for n in c:
            first_graph.node[n]["color"] = rgb2hex(color_map(i))
        i += 1

    # Order of graph is important to keep the color
    composed_graph = nx.compose(second_graph, first_graph)

    contig2color = {}
    contig2color = defaultdict(lambda:"#000000FF", contig2color)

    with open(out_csv, "w") as outfile:
        out_csv_writer = csv.writer(outfile, delimiter=",")
        out_csv_writer.writerow(["Node name", "color"])
        
        for node in composed_graph.nodes():
            out_csv_writer.writerow([node, composed_graph.node[node]["color"]])
            
            if canu_tig_file != "" and (node in id2name or canu_mode):
                node_canu_id = id2name[node]
                if node_canu_id in read2info:
                    tig_id = read2info[node_canu_id]["tigID"]
                    if contig2color[tig_id] == "#000000FF":
                        contig2color[tig_id] = composed_graph.node[node]["color"]

    with open(out_legend, "w") as outfile:
        out_csv_writer = csv.writer(outfile, delimiter=",")
        out_csv_writer.writerow(["Tig name", "color"])
        for tig_name, color in contig2color.items():
            out_csv_writer.writerow([tig_name, color])
