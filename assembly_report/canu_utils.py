import os

import itertools
import csv
from collections import Counter, defaultdict

import gfapy

def project_name(dir_path):

    res = Counter([entry.name.split(".")[0] for entry in os.scandir(dir_path)
                   if entry.is_file()])
        
    if not res:
        raise Exception("This dir didn't look like an canu output dir")

    return max(res, key=res.get)


##########
# WARNING
#########
#
# Big code duplication with script basename2graphename.py
import csv
def parse_canu_read_name(filename):

    id2readname = dict()
    with open(filename, "r") as in_csv:
        reader = csv.reader(in_csv, delimiter='\t')
        for row in reader:
            id2readname[row[0]] = row[1].split(" ")[0]
    
    return id2readname

def keep_tig(canu_tig_info):
    with open(canu_tig_info) as fh:
        reader = csv.DictReader(fh, delimiter="\t")
        for row in reader:
            if row["tigClass"] == "contig":
                yield row["#tigID"]


def get_tigs2readsinfo(filename, id2name):
    ret = defaultdict(list)

    with open(filename, "r") as fd:
        reader = csv.DictReader(fd, delimiter="\t")
        for row in reader:
            if int(row["bgn"]) < int(row["end"]):
                ret[row["tigID"]].append((
                    id2name[row["#readID"]], 
                    int(row["bgn"]),
                    int(row["end"]),
                    "+"
                ))
            else:
                ret[row["tigID"]].append((
                    id2name[row["#readID"]], 
                    int(row["end"]),
                    int(row["bgn"]),
                    "-"
                ))
    
    return ret


def first_valid_read(readsinfo, valid):
    for read in readsinfo:
        if read[0] in valid:
            return read

def reads_valid(gfa, id2name):
    with open(gfa, "r") as fh:
        for line in fh:
            if not line.startswith("S"):
                continue
            line = line.split("\t")
            yield line[1]

def generate_output_extract_graph(contig_layout, id2name, tigInfo, prefix, gfa):
    id2name = parse_canu_read_name(id2name)
    tigs2readsinfo = {k: v for k, v in get_tigs2readsinfo(contig_layout, id2name).items() if len(v) > 1} 
    valid = set(reads_valid(gfa, id2name))

    search_path = prefix + "_extremity_search.csv"
    result_path = prefix + "_extremity_result.csv"

    with open(search_path, "w") as out_file:
        writer = csv.writer(out_file, delimiter=",")
        writer.writerow(["tigA", "readA", "tigB", "readB"])
        for tigA, tigB in itertools.product(tigs2readsinfo.keys(), tigs2readsinfo.keys()):
            if tigA == tigB :
                continue

            a_beg = first_valid_read(sorted(tigs2readsinfo[tigA], key=lambda x: x[1]), valid)
            a_end = first_valid_read(sorted(tigs2readsinfo[tigA], key=lambda x: x[2], reverse=True), valid)
            b_beg = first_valid_read(sorted(tigs2readsinfo[tigB], key=lambda x: x[1]), valid)
            b_end = first_valid_read(sorted(tigs2readsinfo[tigB], key=lambda x: x[2], reverse=True), valid)

            if a_beg is None or a_end is None or b_beg is None or b_end is None:
                continue

            a_beg = a_beg[0] + "-" if a_beg[3] == "+" else a_beg[0] + "+"
            a_end = a_end[0] + "+" if a_end[3] == "+" else a_end[0] + "-"
            b_beg = b_beg[0] + "+" if b_beg[3] == "+" else b_beg[0] + "-"
            b_end = b_end[0] + "-" if b_end[3] == "+" else b_end[0] + "+"

            writer.writerow([tigA, a_beg, tigB, b_beg])
            writer.writerow([tigA, a_beg, tigB, b_end])
            writer.writerow([tigA, a_end, tigB, b_beg])
            writer.writerow([tigA, a_end, tigB, b_end])

    return result_path, id2name
