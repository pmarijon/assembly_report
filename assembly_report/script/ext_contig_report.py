#!/usr/bin/env python3

import io
import sys
import csv
import pandas
import argparse
import subprocess

# For support graph generation without X server
# source http://matplotlib.org/faq/howto_faq.html#matplotlib-in-a-web-application-server
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

import networkx as nx

from collections import defaultdict

class Overlap:

    def __init__(self, ov_bin, ov_store, gk_store, error):
        self._ov_bin = ov_bin
        self._ov_store = ov_store
        self._gk_store = gk_store
        self._error = error

    def _get_all_overlap(self, index, ext5=True):
  
        cmd = [self._ov_bin, "-O", self._ov_store, "-G", self._gk_store, 
               "-d", str(index)]
        cmd.append("-d5") if ext5 else cmd.append("-d3")
        
        process = subprocess.Popen(cmd,
                                   stdout=subprocess.PIPE,
                                   stderr=subprocess.PIPE,
                                   universal_newlines=True)

        out, _ = process.communicate()
        return out

    def best_ov(self, index, ext5=True):
        ov = pandas.read_csv(io.StringIO(self._get_all_overlap(index, ext5)),
                             names=["id", "id_b", "flipped", "span", "beg",
                                    "end", "beg_b", "end_b", "error"],
                             sep="\s+")
 
               # remove containment overlap
        len_read = ov["end"].max()
        for i in ov.index:
            if ext5 :
                if not ov.loc[i]["end"] == len_read :
                    ov.loc[i] = None
            else:
                if not ov.loc[i]["beg"] == 0:
                    ov.loc[i] = None
        ov.dropna(0, inplace=True)

        if ov.empty:
            return {"id": -1.0, "id_b": -1.0, "flipped": -1.0, "span": -1.0,
                  "beg": -1.0, "end": -1.0, "beg_b": -1.0, "end_b": -1.0,
                  "error": -0.0}

        # sort
        first_col = "beg" if ext5 else "end"
        sort_order = [True, False, True] if ext5 else [False, True, True]
        ov.sort_values([first_col, "span", "error"], ascending=sort_order,
                       inplace=True) 

        i = 0
        for i in range(len(ov.index)):
            if ov.loc[ov.index[i]]["error"] <= self._error:
                break

        if i+1 == len(ov.index):
            return ov.loc[ov.index[0]]

        return ov.loc[ov.index[i]]

##########
# WARNING
#########
#
# Big code duplication with script basename2graphename.py
import csv
def parse_canu_read_name(filename, invert):

    index = (0) if invert else (1)
  
    id2readname = dict()
    with open(filename, "r") as in_csv:
        reader = csv.reader(in_csv, delimiter='\t')
        for row in reader:
            id2readname[row[0]] = row[1].split(" ")[0]
    
    id2readname = {v: k for k, v in id2readname.items()} if invert else id2readname
    
    return id2readname


def build_correspondance_function(filename, invert):
    id2readname = parse_canu_read_name(filename, invert)
    
    if not invert:
        def corres_func(iid):
            return str(id2readname[clean_read_name(iid[:-1])]) + iid[-1]
    else:
        def corres_func(iid):
            return id2readname[clean_read_name(iid[:-1])] + iid[-1]

    return corres_func

def clean_read_name(name):
    return name.replace("read", "").lstrip('0')


def read_contig_layout(filename):
    import csv

    read2info = defaultdict(dict)
    tig2extremity = defaultdict(set)
    with open(filename, "r") as fd:
        iterator = csv.reader(fd, delimiter="\t")
        
        key = iterator.__next__()
        key.pop(0)

        actual_tig = 0
        for row in iterator:
            if row[3] > row[4]:
                row[3], row[4] = row[4], row[3]

            # index all info
            for k, v in zip(key, row[1:]):
               read2info[row[0]][k] = v
               
            #index extremity
            if actual_tig == 0:
                actual_tig = row[1]
                actual_begin = row[0]
                actual_end = row[0]
 
            if actual_tig != row[1]:
                tig2extremity[actual_tig] = (actual_begin, actual_end)
                actual_tig = row[1]
                actual_begin = row[0]
                actual_end = row[0]   

            if int(row[4]) > int(read2info[actual_end]["end"]):
                actual_end = row[0]

    return read2info, tig2extremity

def __search_contig_extremity(overlap, read2info, n):
    nP = str(int(overlap.best_ov(n, True)["id_b"]))
    nM = str(int(overlap.best_ov(n, False)["id_b"]))
        
    ret = list()
    if nP in read2info and read2info[nP]["tigID"] == read2info[n]["tigID"]:
        return "-"
    elif nM in read2info and read2info[nM]["tigID"] == read2info[n]["tigID"]:
        return "+"
    else:
        raise Exception("We can't find free read extremity for "+n)


def main(args):

    parser = argparse.ArgumentParser(prog="paf2gfa",
                                     formatter_class=argparse.
                                     ArgumentDefaultsHelpFormatter)
    parser.add_argument("read2tig", help="canu read to tig file")
    parser.add_argument("ovBinary", help="ovStoreDump binary path")
    parser.add_argument("ovStore", help="ovStore dir path")
    parser.add_argument("gkpStore", help="gatekeeper dir path")
    parser.add_argument("outBaseName", help="The prefix of name generate by this program")
    parser.add_argument("--error", help="error rate", default = 0.0045)

    arg = vars(parser.parse_args(args))

    read2tig       = arg["read2tig"]
    ovBinary       = arg["ovBinary"]
    ovStore        = arg["ovStore"]
    gkpStore       = arg["gkpStore"]
    outBaseName    = arg["outBaseName"] 
    error          = arg["error"]
    
    overlap = Overlap(ovBinary, ovStore, gkpStore, error)

    col_name = ["tig", "read name", "free extremity", "read name in canu",
                "overlap error", "span"]
    result = list()
    overlap_series = list()
    span_series = list()

    read2info, tig2extremity = read_contig_layout(read2tig)
    for tig, reads in tig2extremity.items():
        if reads[0] == reads[1]:
            continue

        for read in reads:
            try:
                extremity = __search_contig_extremity(overlap, read2info, read)
            except:
                extremity = "None"
            
            line = list()
            line.append(tig)
            line.append(read)
            line.append(extremity)
            if extremity == "+":
                ov = overlap.best_ov(read)
            elif extremity == "-":
                ov = overlap.best_ov(read, False)
            else:
                ov = overlap.best_ov(read)

            line.append(ov["id_b"])
            line.append(ov["error"])
            overlap_series.append(ov["error"])

            line.append(ov["span"])
            span_series.append(ov["span"])

            result.append(line)

    fig, axes = plt.subplots(ncols=2, figsize=(16, 6))

    hist_error = pandas.Series(overlap_series).hist(ax=axes[0])
    hist_error.set_xlabel("error rate")
    hist_error.set_ylabel("number of overlaps")
    hist_error.set_title("Error rate at contig free extremity")

    hist_span = pandas.Series(span_series).hist(ax=axes[1])
    hist_span.set_xlabel("overlap span length")
    hist_span.set_ylabel("number of overlaps")
    hist_span.set_title("Overlap span rate at contig free extremity")

    fig.savefig(outBaseName+".png", dpi=100)

    with open(outBaseName + ".csv", "w") as fd:
        writer = csv.writer(fd, delimiter=",")
        writer.writerow(col_name)
        for line in result:
            writer.writerow(line)


if __name__ == "__main__":
    main(sys.argv[1:])
