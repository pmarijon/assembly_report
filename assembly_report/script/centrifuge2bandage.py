#!/usr/bin/env python

import begin
import pandas

from matplotlib import cm
from matplotlib import colors
from matplotlib.colors import rgb2hex

def color_map_gene(n: int):
    
    color_norm = colors.Normalize(vmin=0, vmax=n-1)
    scalar_map = cm.ScalarMappable(norm=color_norm, cmap='hsv')

    def map_index_to_rgb_color(index: int):
        return scalar_map.to_rgba(index)

    return map_index_to_rgb_color

@begin.start
def main(centrifuge: "centrifuge tsv output", out: "csv bandage compatible out"):
   
    centri_assign = pandas.read_csv(centrifuge, sep="\t", 
                                    usecols=["readID", "taxID"])

    if centri_assign.empty:
        centri_assign["color"] = list()
    else:
        tax_id = list(set(centri_assign["taxID"]))
        func_id2color = color_map_gene(len(tax_id))
        id2color = {tax_id[i]: rgb2hex(func_id2color(i))
                   for i in range(len(tax_id))}

        centri_assign["color"] = centri_assign.apply(lambda x: id2color[x[1]],
                                                 axis=1)

    centri_assign[["readID", "color"]].to_csv(out, sep=',',
                                              index=False,
                                              header=["Node name", "color"])
