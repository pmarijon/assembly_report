#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

from assembly_report import __version__, __name__

try:
    from pip._internal.req import parse_requirements
except ImportError:
    from pip.req import parse_requirements

from itertools import tee

pip_reqs = parse_requirements("requirements.txt", session=False)
pipy_reqs = [str(ir.req) for ir in pip_reqs if ir.link is None]

git_reqs = parse_requirements("requirements.txt", session=False)
links_reqs = [str(ir.link) for ir in git_reqs if ir.link is not None]

setup(
    name = __name__,
    version = __version__,
    packages = find_packages(),

    author = "Pierre Marijon",
    author_email = "pierre.marijon@inria.fr",
    description = "run Snakemake pipeline for analysis assembly output",
    long_description = open('README.md').read(),
    url = "https://gitlab.inria.fr/pmarijon/assembly_report",
    
    install_requires = pipy_reqs,
    include_package_data = True,
    
    classifiers = [
        "Programming Language :: Python :: 3",
        "Development Status :: 2 - Pre-Alpha"
    ],

    entry_points = {
        'console_scripts': [
            'assembly_report = assembly_report.__main__:main'
        ]
    }
)
