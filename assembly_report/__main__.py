import sys
import csv

import argparse

import os
import io

import assembly_report
from assembly_report import canu_utils

import subprocess

import logging

def main(args = None):

    if args is None:
        args = sys.argv[1:]

    # Argument parsing setting 
    parser = argparse.ArgumentParser(prog="assembly_report",
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("-i", "--input", help="canu directory", default=os.getcwd())
    parser.add_argument("-o", "--output", help="output directory",
                        default=os.path.join(os.getcwd(), "assembly_report"))
    parser.add_argument("-p", "--project", help="project name give to canu, default auto detect use option to overide")
    parser.add_argument("-m", "--minimap", help="path to minimap paf output")
    parser.add_argument("-M", "--miniasm", help="path to miniasm gfa output")
    parser.add_argument("-v", "--verbose", action="store_true", help="verbose")
    parser.add_argument("-c", "--clean", action="store_true", help="clean all file generated")
    parser.add_argument("-t", "--thread", help="number of thread usable", default="1")

    arg = vars(parser.parse_args(args))

    # Logging setting
    logger = logging.getLogger()
    if arg["verbose"]:
        logger.setLevel(logging.DEBUG)
    else:
        logger.setLevel(logging.INFO)
    formatter = logging.Formatter('%(levelname)s :: %(message)s')
    steam_handler = logging.StreamHandler()
    steam_handler.setFormatter(formatter)
    logger.addHandler(steam_handler)

    # Pipeline settings
    package_path = os.path.dirname(assembly_report.__file__)
    config_path = os.path.join(package_path, "config.yaml")

    if arg["project"] is None:
        project_name = canu_utils.project_name(arg["input"])
    else:
        project_name = arg["input"]
 
    # Check if minimap is set when miniasm is set
    if arg["miniasm"] and not arg["minimap"]:
        logger.error("You need set minimap if you set miniasm")
        return 1
   
    # Pipeline calling construction
    call = ["snakemake", "report",
            "--configfile", config_path,
            "--jobs", arg["thread"],
            "-p",
            "--config",
            "out_path="+arg["output"],
            "in_path="+arg["input"],
            "project_name="+project_name,
            "package_path="+package_path]

    # Pipeline running
    if arg["miniasm"]:
        logger.info("Run assembly_report in canu_mini{asm|map} mode")
        snakefile_path = os.path.join(package_path, "canu_miniasm.snakemake")
        call += ["minimap_path="+arg["minimap"],
                 "miniasm_path="+arg["miniasm"],
                 "--snakefile", snakefile_path
                 ]
    elif arg["minimap"]:
        logger.info("Run assembly_report in canu_minimap mode")
        snakefile_path = os.path.join(package_path, "canu_minimap.snakemake")    
        call += ["minimap_path="+arg["minimap"],
                 "--snakefile", snakefile_path]
    else:
        logger.info("Run assembly_report in canu_only mode")
        snakefile_path = os.path.join(package_path, "canu_only.snakemake")
        call += ["--snakefile", snakefile_path]

    if arg["clean"]:
        logger.info("Run clean")
        call.append("-S")
        out = subprocess.run(call, stdout=subprocess.PIPE)
        
        fakefile = io.StringIO(str(out.stdout.decode("utf-8")))
        fakefile.seek(0)
        
        reader = csv.reader(fakefile, delimiter="\t")
        next(reader)
        for row in reader:
            rm_out = subprocess.run(["rm", "-rf", row[0]])
            logger.info(" ".join(rm_out.args))
    else:
        out = subprocess.run(call)
        logger.debug(" ".join(out.args)) 

if __name__ == "__main__":
    main(sys.argv[1:])
