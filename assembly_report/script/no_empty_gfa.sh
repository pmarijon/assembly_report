#!/bin/bash

gfa_content=$(cat $1)

if [ -z $gfa_content ]
then 
    printf 'S\t1\tCGATGCAA\nS\t2\tTGCAAAGTAC\nL\t1\t+\t2\t+\t5M\n' > $2 
else
    cp $1 $2
fi
